from datetime import datetime
from DirectoryWordCount.WordCounter import WordCounter
from flask import jsonify
def count(root,regex):
    response = {}
    is_success = False
    try:
        wc = WordCounter(root,regex)
        counts = wc.count_words()
        is_success = True
        response["counts"] = counts
    
    except Exception as e:
        response["error"] = str(e)
    
    response["success"] = is_success
    return jsonify(response)