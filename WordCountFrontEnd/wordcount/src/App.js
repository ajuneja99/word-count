import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import MenuItem from '@material-ui/core/MenuItem';
import './App.css';
import 'toastr/build/toastr.css';
import * as toastr from 'toastr';

class App extends Component {
  constructor(props) {
    super(props)
    this.submitRequest = this.submitRequest.bind(this);
    var options = {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Word Count'
    },
    xAxis: {
        categories: [
            
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'count'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} count</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'word count',
        data: []

    }]
}
    this.state = {
      graph:options,
      "dir":"",
      "pattern":"[a-zA-Z0-9]*[.]txt",
      "limit":-1
    }

  }



  submitRequest() {
     /**
        - Submit Request to Word Count API
        - Sort words by value
        - Filter results
        - Update State of Histogram 
     **/
      
     //Generate URL for REST Request
      var root_dir = this.state.dir
      var pattern = this.state.pattern
      var url = `http://localhost:5000/api/word-count?root=${root_dir}&regex=${pattern}`

      //Call WordCount API
      fetch(url).then((response)  =>  {
        return response.json();
      }).then((result)  =>  {
        
        //Display Error Messaage if not success
        if(!result.success) {
          toastr.error(result.error)
          return
        }
        //Collect all words from JSON object as a list of tuples [(count,word)]
        var val = []
        Object.keys(result.counts).forEach(function(key) {
          val.push(([Number(result.counts[key]),key]))
        })

        //Sort by first index of tuple
        val = val.sort(function(a, b){return b[0]-a[0]})
        
        //Collect x values
        var x = val.map((item, i, arr) => {
            return item[0];
        });

        //Collect y values
        var y = val.map((item, i, arr) => {
            return item[1];
        });
        
        //Filter results if filter set
        if(this.state.limit >= 0) {
          x = x.slice(0, this.state.limit);
          y = y.slice(0,this.state.limit)
        }

      //Update graph's X and Y state  
      var updatedGraph = this.state.graph
      updatedGraph.xAxis.categories = y
      updatedGraph.series[0].data = x
      this.setState({"graph":updatedGraph})

      }).catch(function() {
         toastr.error("No Response from WordCount API")
    });


      

  }

  render() {
  
    return (
      <div className="App">
            <header className="App-header">
              <img src="https://image.flaticon.com/icons/svg/217/217810.svg" className="App-logo" alt="logo" />
              <h1 className="App-title">Directory Word Explorer</h1>
            </header>
              <h2>Explore a Directory</h2>
              <TextField className="form-element" id="dir" required label="Directory Name" placeholder="documents" margin="normal" value={this.state.dir} onChange={e => this.setState({ dir: e.target.value })}/>
              <TextField className="form-element" id= "pattern" required label="File Pattern" placeholder="documents" margin="normal" value={this.state.pattern} onChange={e => this.setState({ pattern: e.target.value })}/>
              <Select className="form-element" value={this.state.limit} onChange={e => this.setState({ limit: e.target.value })}>
                  <MenuItem value={-1}>No Limit</MenuItem>
                  <MenuItem value={10}>Top 10 Words</MenuItem>
                  <MenuItem value={20}>Top 20 Words</MenuItem>
                  <MenuItem value={50}>Top 50 words</MenuItem>
              </Select>
              <Button className="form-element" variant="contained" color="secondary" onClick={this.submitRequest}>Submit</Button>       
              <HighchartsReact highcharts={Highcharts} options={this.state.graph}/>
      </div>
    );
  }
}

export default App;
