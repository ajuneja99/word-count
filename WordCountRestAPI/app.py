import flask
import connexion
from flask_cors import CORS, cross_origin

app = connexion.App(__name__, specification_dir='./')

#configure swagger endpoints
app.add_api('swagger.yaml')
webapp = app.app
CORS(webapp) #add allow/disallow rules if certain domains should be restricted from consuming the service
@app.route('/')
def home():
    return ""

# If we're running in stand alone mode, run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)