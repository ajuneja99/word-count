# Directory Explorer
The project is broken into three components

  - A Python WordCount package that processes directories and returns counts
  - A REST service that uses the WordCount package to respond to queries about various directories and filenames
  - A React front end that consumes the REST service and generates a histogram
  - Video Demo: https://streamable.com/ckbfg
  - Requires Python3. Run all ppython commands with pip, python if default version is 3.0+ or else use pip3,python3 etc.

## WordCount Package
  - Contains the core logic of traversing files and counting words as a python package
  - This allows easy installation in locally operated package repositories like pip and Nexus.
  - Allows easy rollout of new features and bug fixes from consultants, as well as options for other teams in the organization to use the same library
  - Decouples the UI and business logic
  - The library counts words in any file that matches the input regex. The default is all ".txt" files as stated in the description but this can be easily extended to other filetypes and more complex filename patterns if requirements shift or if other use cases emerge 
  - cd WordCount & run python3 DirectoryWordCountTest.py 
  - This contains a few basic tests to ensure the library works as expected on your local machine
  ![N|Solid](unit_test_result.png)

## WordCount REST API
[![N|Solid](https://image.ibb.co/cudavK/swagger.png)]()
#### Run REST-API
    1. cd WordCountRestAPI
    2. pip install -r requirements.txt OR pip3 install -r requirements.txt
    3. python app.py OR python3 app.py
    4. Point browser to http://localhost:5000/api/ui/

## WordCount UI
[![N|Solid](https://image.ibb.co/b9WsSz/web_ui.png)]()
#### Run UI From Docker Container
    1. cd WordCountFrontEnd/wordcount
    2. docker build -t wc-frontend .
    3. docker run -it -v ${PWD}:/usr/src/app -v /usr/src/app/node_modules -p 5001:3000 --rm wc-frontend
    4. Point browser to URL of local network displayed 
#### Run UI From Local
    1. cd WordCountFrontEnd/wordcount
    2. npm install
    3. npm start
